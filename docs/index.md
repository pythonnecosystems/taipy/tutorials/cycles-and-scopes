# 주기(cycle)와 범위(scope)

> 코드는 [여기서](https://docs.taipy.io/scope_and_cycle.py) 다운받을 수 있다. 여기에 [TOML 파일](https://docs.taipy.io/config.toml)이 있는 [Python 프로그램](https://docs.taipy.io/scope_and_cycle_toml.py)이다. 두 개의 데이터세트도 다운받아야 한다([여기](https://docs.taipy.io/config.toml)와 [여기](https://docs.taipy.io/config.toml)).

본 절에서는 Taipy에서 데이터 노드와 시나리오를 효과적으로 관리하는 데 도움이 되는 두 가지 핵심 개념인 [Scope](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/scope/)와 [Cycle](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/cycle/) 간의 복잡한 관계를 살펴볼 것이다.

## 주기(cycle)
주기는 기업이 자주 접하는 비즈니스 상황을 반영하기 위해 도입되었으며 매우 유용할 수 있다.

예를 들어, 한 대형 패스트푸드 체인점은 매주 매장 매출을 예측하려고 한다. 그들이 특정 주에 대한 시나리오를 만들 때, 해당 주에 연결되어야 한다. 보통 특정 주에 만들어진 모든 시나리오에 사용되는 시나리오는 하나뿐이다. 이 특별한 '공식' 시나리오를 Taipy에서는 'Primary' 시나리오라고 부른다.

비즈니스 문제에 시간 빈도가 없는 경우 사이클을 완전히 무시할 수 있다.

**주기의 장점**

- **시간 기반 조직(Time-Based Organization)**: 주기는 특정 기간에 대한 시나리오의 구성과 분석을 용이하게 한다.
- **기본 시나리오 식별(Primary Scenario Identofocation)**: 주기를 통해 참조 또는 공식 분석을 위한 기본 시나리오를 지정할 수 있다.
- **향상된 데이터 일관성(Enhanced Data Consistency)**: 특정 기간에 연결된 시나리오 간에 데이터와 매개 변수 일관성을 보장한다.
- **커스텀 주파수(Custom Frequency)**: 특정 비즈니스 요구에 맞게 다양한 빈도에 맞게 사이클을 커스터마이징할 수 있다.
- **시나리오 비교(Scenario Comparison)**: 주기을 사용하면 동일한 시간 내에 서로 다른 시나리오와 쉽게 비교 분석할 수 있다.

### 예: 월별 필터링

```python
def filter_by_month(df, month):
    df['Date'] = pd.to_datetime(df['Date']) 
    df = df[df['Date'].dt.month == month]
    return df
```

![](./config.svg)

### 구성
빈도에 대한 시나리오 구성에 파라미터가 추가된다.

 ```python
 from taipy.config import Frequency

historical_data_cfg = Config.configure_csv_data_node(id="historical_data",
                                                     default_path="time_series.csv")

month_cfg =  Config.configure_data_node(id="month")
month_values_cfg =  Config.configure_data_node(id="month_values")

task_filter_cfg = Config.configure_task(id="filter_by_month",
                                        function=filter_by_month,
                                        input=[historical_data_cfg, month_cfg],
                                        output=month_values_cfg)


scenario_cfg = Config.configure_scenario(id="my_scenario",
                                         task_configs=[task_filter_cfg],
                                         frequency=Frequency.MONTHLY)ㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊㅊ
 ```

 frequency=Frequency.MONTLEY를 지정했기 때문에 생성 시 해당 시나리오는 정확한 기간(월)에 자동으로 첨부됩니다.

시나리오가 속한 주기는 시나리오의 `creation_date`를 기준으로 한다. 다음 예에서 하는 것처럼 `creation_date`를 수동으로 설정하여 특정 주기에 "부착"할 수 있다.

```python
tp.Core().run()

scenario_1 = tp.create_scenario(scenario_cfg,
                                creation_date=dt.datetime(2022,10,7),
                                name="Scenario 2022/10/7")
scenario_2 = tp.create_scenario(scenario_cfg,
                                creation_date=dt.datetime(2022,10,5),
                                name="Scenario 2022/10/5")
```

`scenario_1`과 `scenario_2`는 동일한 시나리오 구성을 사용하여 생성된 두 개의 별개의 시나리오 객체이다. 이들은 동일한 주기에 속하지만 서로 다른 데이터 노드를 갖는다. 기본적으로 각 시나리오 인스턴스는 고유한 데이터 노드 인스턴스를 가지며 다른 시나리오와 공유되지 않는다.

### 범위와 주기 간 상호 작용
주기들은 시나리오들의 `create_date`에 따라 생성된다. 반면에, 범위는 이러한 주기와 시나리오 내에서 데이터 노드들이 어떻게 공유되는지를 결정한다.

## 범위(Scope)
객체 간에 데이터 노드를 공유하면 데이터를 더 잘 구성하고 관리할 수 있다. 데이터 중복을 방지하고 Taipy가 실행을 더 잘 관리할 수 있다([생략 가능한 작업](https://docs.taipy.io/en/release-3.0/knowledge_base/tips/skippable_tasks/) 참조). 개발자는 다음과 같이 결정할 수 있다.

- `Scope.Scenario` (기본값): 각 시나리오에 하나의 데이터 노드가 있다.
- `Scope.CYCLE`: 주어진 주기의 모든 시나리오에 걸쳐 데이터 노드를 공유하여 범위를 확장한다.
- `Scope.GLOBAL`: 범위를 전역으로 확장하여 모든 주기의 모든 시나리오에 적용한다.

데이터 노드의 범위를 수정하는 것은 간단하다. 데이터 노드의 구성을 변경해 보겠다.

- *historical_data*: 는 전역 (global) 데이터 노드이다. 매 주기와 시나리오별로 공유될 것이다.
- *month*: 는 주기 데이터 노드이다. 같은 달의 모든 시나리오가 이 데이터를 공유한다.
- *month_values*: *month_values*에 대해 동일하다.

![](./config_scope.svg)

구성은 데이터 노드 구성을 제외하고는 마지막 단계와 동일하다. 범위에 대한 새로운 파라미터가 추가된다.

```python
from taipy.config import Frequency, Scope

historical_data_cfg = Config.configure_csv_data_node(id="historical_data",
                                                     default_path="time_series.csv",
                                                     scope=Scope.GLOBAL)

month_cfg =  Config.configure_data_node(id="month", scope=Scope.CYCLE)
month_values_cfg =  Config.configure_data_node(id="month_values", scope=Scope.CYCLE)
```

`scenario_1`의 `month`를 정의하면 동일한 데이터 노드를 공유하기 때문에 `scenario_2`의 `month`도 결정된다.

```python
scenario_1.month.write(10)


print("Month Data Node of Scenario 1:", scenario_1.month.read())
print("Month Data Node of Scenario 2:", scenario_2.month.read())

scenario_1.submit()
scenario_2.submit()
```

출력:

```
Month Data Node of Scenario 1: 10
Month Data Node of Scenario 2: 10
```

두 시나리오가 모두 동일한 주기에 있고 모든 데이터 노드가 적어도 주기 범위를 가지고 있는 이 특이한 예에서 하나를 실행하는 것은 모든 데이터 노드를 공유하는 것과 동일하다.

## 주기에 대하여 더 나아가기

### 기본(primary) 시나리오
각각의 주기에는 기본 시나리오가 존재한다. 기본 시나리오는 주기의 중요한 시나리오, 즉 기준을 나타낸다는 점에서 흥미롭다. 기본적으로 주기에 대해 생성된 첫 번째 시나리오가 기본 시나리오이다.

#### 기본 시나리오와 관련된 파이썬 코드
[`tp.set_primary(<Scenario>)`](https://docs.taipy.io/en/release-3.0/manuals/core/entities/scenario-cycle-mgt/#promote-a-scenario-as-primary)를 사용하면 주기에서 기본 시나리오를 변경할 수 있다.

`<Scenario>.is_primary`는 시나리오가 기본 시나리오인지 아닌지를 식별한다.

```python
before_set_1 = scenario_1.is_primary
before_set_2 = scenario_2.is_primary

tp.set_primary(scenario_2)

print('Scenario 1: Primary?', before_set_1, scenario_1.is_primary)
print('Scenario 2: Primary?', before_set_2, scenario_2.is_primary)
```

출력:

```
Scenario 1: Primary? True False
Scenario 2: Primary? False True
```

### 주기에서 유용한 기능

- `tp.get_primary_scenarios()`: 모든 기본 시나리오 목록을 반환한다
- `tp.get_scenaries(cycle=<Cycle>)`: `Cycle`의 모든 시나리오를 반환한다.
- `tp.get_cycle()`:` Cycle` 리스트를 반환한다
- `tp.get_primary(<Cycle>)`: `Cycle`의 기본 시나리오를 반환한다

#### 시나리오 관리 시각적 요소
주기를 시나리오 관리 시각적 요소를 통해 제어할 수 있다. 주기는` scenario_selector` 또는 `data_node_selector`에서 볼 수 있다. 또한 시나리오 시각적 요소를 통해 직접 시나리오를 기본으로 지정할 수도 있다.

```python
data_node = None
scenario = None

tp.Gui("""<|{scenario}|scenario_selector|>
          <|{scenario}|scenario|>
          <|{scenario}|scenario_dag|>
          <|{data_node}|data_node_selector|>""").run()
```

### 마치며
개발자는 범위와 주기 사이의 역학 관계를 이해함으로써 특정 비즈니스 요구와 시나리오에 맞게 데이터 노드와 시나리오를 효과적으로 관리할 수 있다. 다양한 구성으로 실험하여 기능과 어플리케이션에 대한 더 깊은 통찰력을 얻을 수 있다.

## 전체 코드

```python
from taipy.config import Config, Frequency, Scope
import taipy as tp
import datetime as dt
import pandas as pd


def filter_by_month(df, month):
    df['Date'] = pd.to_datetime(df['Date']) 
    df = df[df['Date'].dt.month == month]
    return df


historical_data_cfg = Config.configure_csv_data_node(id="historical_data",
                                                     default_path="time_series.csv",
                                                     scope=Scope.GLOBAL)
month_cfg =  Config.configure_data_node(id="month",
                                        scope=Scope.CYCLE)
month_values_cfg =  Config.configure_data_node(id="month_data",
                                               scope=Scope.CYCLE)


task_filter_cfg = Config.configure_task(id="filter_by_month",
                                        function=filter_by_month,
                                        input=[historical_data_cfg, month_cfg],
                                        output=month_values_cfg)


scenario_cfg = Config.configure_scenario(id="my_scenario",
                                         task_configs=[task_filter_cfg],
                                         frequency=Frequency.MONTHLY)


if __name__ == '__main__':
    tp.Core().run()

    scenario_1 = tp.create_scenario(scenario_cfg,
                                    creation_date=dt.datetime(2022,10,7),
                                    name="Scenario 2022/10/7")
    scenario_2 = tp.create_scenario(scenario_cfg,
                                    creation_date=dt.datetime(2022,10,5),
                                    name="Scenario 2022/10/5")

    scenario_1.month.write(10)

    print("Month Data Node of Scenario 1:", scenario_1.month.read())
    print("Month Data Node of Scenario 2:", scenario_2.month.read())

    scenario_1.submit()

    before_set_1 = scenario_1.is_primary
    before_set_2 = scenario_2.is_primary

    tp.set_primary(scenario_2)

    print('Scenario 1: Primary?', before_set_1, scenario_1.is_primary)
    print('Scenario 2: Primary?', before_set_2, scenario_2.is_primary)

    scenario = None
    data_node = None

    tp.Gui("""<|{scenario}|scenario_selector|>
              <|{scenario}|scenario|>
              <|{scenario}|scenario_dag|>
              <|{data_node}|data_node_selector|>""").run()
```

